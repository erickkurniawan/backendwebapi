﻿using BackeendApps.DAL;
using BackeendApps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace BackeendApps.Controllers
{
    public class ProductController : ApiController
    {
        private ProductDAL productDAL;
        private ProductEFDAL productEFDAL;
        public ProductController()
        {
            productDAL = new ProductDAL();
            productEFDAL = new ProductEFDAL();
        }

        // GET: api/Product
        public IEnumerable<Product> Get()
        {
            return productEFDAL.GetAll();
        }

        // GET: api/Product/5
        public async Task<Product> Get(string id)
        {
            return await productDAL.GetById(id);
        }

        [Route("api/Product/GetMyProduct")]
        [HttpPost]
        public Product GetMyProduct(Product product)
        {
            return product;
        }

        // POST: api/Product
        public async Task<IHttpActionResult> Post([FromBody]Product product)
        {
            try
            {
                var result = await productDAL.Insert(product);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT: api/Product/5
        public async Task<IHttpActionResult> Put(string id, [FromBody]Product product)
        {
            try
            {
                await productDAL.Update(id, product);
                return Ok("Data berhasil diupdate");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [Route("api/Product/HelloAPI")]
        [HttpGet]
        public string HelloAPI()
        {
            return "Hello Web API";
        }

        // DELETE: api/Product/5
        public async Task<IHttpActionResult> Delete(string id)
        {
            try
            {
                await productDAL.Delete(id);
                return Ok("Data berhasil didelete");
            }
            catch (Exception ex)
            {
                return BadRequest("Error: " + ex.Message);
            }
        }
    }
}
