﻿using BackeendApps.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Threading.Tasks;

namespace BackeendApps.DAL
{
    public class ProductDAL
    {
        private string connStr;
        private SqlConnection conn;
        private SqlCommand cmd;
        private SqlDataReader dr;

        public ProductDAL()
        {
            connStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        public async Task<IEnumerable<Product>> GetAll()
        {
            List<Product> lstProduct = new List<Product>();
            using (conn = new SqlConnection(connStr))
            {
                string strSql = @"select * from Products 
                                  order by ProductName asc";
                cmd = new SqlCommand(strSql, conn);
                conn.Open();
                dr = await cmd.ExecuteReaderAsync();
                if (dr.HasRows)
                {
                    while (await dr.ReadAsync())
                    {
                        lstProduct.Add(new Product
                        {
                            ProductID = Convert.ToInt32(dr["ProductID"]),
                            ProductName = dr["ProductName"].ToString(),
                            Quantity = Convert.ToInt32(dr["Quantity"]),
                            Price = Convert.ToDecimal(dr["Price"])
                        });
                    }
                }
                dr.Close();
                cmd.Dispose();
                conn.Close();
            }
            return lstProduct;
        }

        public async Task<Product> GetById(string id)
        {
            Product product = new Product();
            using (conn = new SqlConnection(connStr))
            {
                string strSql = @"select * from Products 
                                  where ProductID=@ProductID";
                cmd = new SqlCommand(strSql, conn);
                cmd.Parameters.AddWithValue("@ProductID", id);
                conn.Open();
                dr = await cmd.ExecuteReaderAsync();
                if (dr.HasRows)
                {
                    await dr.ReadAsync();
                    product.ProductID = Convert.ToInt32(dr["ProductID"]);
                    product.ProductName = dr["ProductName"].ToString();
                    product.Quantity = Convert.ToInt32(dr["Quantity"]);
                    product.Price = Convert.ToDecimal(dr["Price"]);
                }
                dr.Close();
                cmd.Dispose();
                conn.Close();
            }
            return product;
        }

        public async Task<Product> Insert(Product product)
        {
            using (conn = new SqlConnection(connStr))
            {
                string strSql = @"insert into Products(ProductName,Quantity,Price) 
                                  values(@ProductName,@Quantity,@Price);select @@identity";
                cmd = new SqlCommand(strSql, conn);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@ProductName", product.ProductName);
                cmd.Parameters.AddWithValue("@Quantity", product.Quantity);
                cmd.Parameters.AddWithValue("@Price", product.Price);

                try
                {
                    conn.Open();
                    int id =  Convert.ToInt32(await cmd.ExecuteScalarAsync());
                    product.ProductID = id;
                    return product;
                }
                catch (SqlException sqlEx)
                {
                    throw new Exception(sqlEx.Message);
                }
                finally
                {
                    cmd.Dispose();
                    conn.Close();
                }
            }
        }

        public async Task Update(string id, Product product)
        {
            using (conn = new SqlConnection(connStr))
            {
                string strSql = @"update Products set ProductName=@ProductName, 
                                  Quantity=@Quantity,Price=@Price  
                                  where ProductID=@ProductID";
                cmd = new SqlCommand(strSql, conn);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@ProductName", product.ProductName);
                cmd.Parameters.AddWithValue("@Quantity", product.Quantity);
                cmd.Parameters.AddWithValue("@Price", product.Price);
                cmd.Parameters.AddWithValue("@ProductID", id);

                try
                {
                    conn.Open();
                    await cmd.ExecuteNonQueryAsync();
                }
                catch (SqlException sqlEx)
                {
                    throw new Exception(sqlEx.Message);
                }
                finally
                {
                    cmd.Dispose();
                    conn.Close();
                }
            }
        }

        public async Task Delete(string id)
        {
            using (conn = new SqlConnection(connStr))
            {
                string strSql = @"delete from Products 
                                  where ProductID=@ProductID";
                cmd = new SqlCommand(strSql, conn);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@ProductID", id);

                try
                {
                    conn.Open();
                    await cmd.ExecuteNonQueryAsync();
                }
                catch (SqlException sqlEx)
                {
                    throw new Exception(sqlEx.Message);
                }
                finally
                {
                    cmd.Dispose();
                    conn.Close();
                }
            }

        }
    }
}