﻿using BackeendApps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackeendApps.DAL
{
    public class ProductEFDAL
    {
        private ApplicationDbContext db;
        public ProductEFDAL()
        {
            db = new ApplicationDbContext();
        }

        public IEnumerable<Product> GetAll()
        {
            //var products = from p in db.Products
            //               orderby p.ProductName
            //               select p;
            var products = db.Products.OrderBy(p => p.ProductName);

            return products;
        }
    }
}